import Model.Asteroid;
import Model.Model;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;


public class Graphics {
    private Model model;
    private GraphicsContext gc;

    public Graphics(Model model, GraphicsContext gc) {
        this.model = model;
        this.gc = gc;
    }

    public void draw() {
        gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);
        //gc.setFill(Color.BLACK);
        //gc.fillRect(0,0,Model.WIDTH, Model.HEIGHT);
        Image hintergrund = new Image("Bilder/hintergrund2.png");
        gc.drawImage(hintergrund, 0, 0);


        //gc.setFill(Color.BLUE);
        //gc.fillRect(model.schiff.getX() - 40 / 2, model.schiff.getY() - 30 / 2, 40, 30);
        Image spaceship = new Image("Bilder/spaceshipZero.png");
        gc.drawImage(spaceship,model.schiff.getX() - 40 / 2, model.schiff.getY() - 30 / 2, 40, 30);

        //gc.drawImage(spaceship, model.schiff.getX(), model.schiff.getY(),40,30);

        //gc.fillRect(Color.BLUE);
        //gc.fillRect(model.schiff.getX(), model.schiff.getY());


        for (Asteroid asteroid : model.getAsteroids()) {
            Image gestein = new Image("Bilder/gestein.png");
            //gc.setFill(Color.BROWN);
            gc.drawImage(gestein, asteroid.getX(), asteroid.getY(), 30, 30);
            //gc.fillOval(asteroid.getX(),asteroid.getY(),30,30);
        }


    }

}
