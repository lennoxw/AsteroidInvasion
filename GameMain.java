import Model.Model;
import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class GameMain extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        Timer timer;



        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);


        Group group = new Group();
        group.getChildren().add(canvas);


        Scene scene = new Scene(group, 600, 800);



        stage.setScene(scene);
        stage.show();

        GraphicsContext gc = canvas.getGraphicsContext2D();

        //GraphicsContext gc = canvas.getGraphicsContext2D();
        Model model = new Model();
        Graphics graphics = new Graphics(model,gc);
        timer = new Timer(model, graphics);
        timer.start();

        InputHandler inputHandler = new InputHandler(model);
        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );







    }

   /* public static void main(String[] args) {
        //launch(args);
    }*/
}
