import Model.Model;
import javafx.scene.input.KeyCode;

public class InputHandler {


    // Eigenschaften
    private Model model;

    // Konstruktoren
    public InputHandler (Model model) {
        this.model = model;
    }

    // Methoden
    public void onKeyPressed(KeyCode key) {
        /*if (key == KeyCode.UP) {
            model.getSchiff().move(0,-10);
        }
        else if(key == KeyCode.DOWN) {
            model.getSchiff().move(0,10);
        }*/
         if(key == KeyCode.LEFT) {
             if (model.schiff.getX() + 15 > 590 || model.schiff.getX() - 15 < 10) {
                 model.schiff.setX(580);
             }
            model.getSchiff().move(-15,0);
        }
        else if(key == KeyCode.RIGHT) {
             if (model.schiff.getX() + 15 > 590 || model.schiff.getX() - 15 < 10) {
                 model.schiff.setX(20);
             }
            model.getSchiff().move(15,0);
        }
    }


}
