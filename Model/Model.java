package Model;



import java.awt.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;


public class Model {

    public static final int WIDTH = 600;
    public static final int HEIGHT = 800;

    public List<Asteroid> asteroids = new LinkedList<>();
    public SpaceShip schiff ;


    public Model() {
        this.schiff = new SpaceShip(300);
        this.asteroids.add(new Asteroid(250, 0, 0.2f));
        this.asteroids.add(new Asteroid(110, 0,0.2f));
        this.asteroids.add(new Asteroid(220, 0,0.2f));
        this.asteroids.add(new Asteroid(400, 0,0.3f));
        this.asteroids.add(new Asteroid(150, 0,0.2f));
        this.asteroids.add(new Asteroid(400, 0,0.15f));
        this.asteroids.add(new Asteroid(300, 0,0.2f));
        this.asteroids.add(new Asteroid(200, 0,0.2f));


    }

    public void update (long elapsedTime) {
        Random random = new Random();
        int randx = (int) (Math.random() * 550 + 30);
        Rectangle schiffKollision = this.schiff.getSpaceShipBounds();
        for (Asteroid asteroid : asteroids){
           /* if (asteroid.collision(getSchiff())) {
                System.out.println("Kollision");
                System.exit(0);
            }*/
            Rectangle kollisionsAsteroid = asteroid.getAsteroidBounds();
            if (schiffKollision.intersects(kollisionsAsteroid)) {
                System.exit(10);
            }
            asteroid.update(elapsedTime);

        }
        //if (randx % 5 == 0) {
            //this.asteroids.add(new Asteroid(610, 0, 0.20f));
            //this.asteroids.add(new Asteroid(-30, 0, 0.20f));
        //}
         if ((random.nextInt(1000) + 1) % 20 == 0) {
            this.asteroids.add(new Asteroid(randx, 0, 0.23f));
             this.asteroids.add(new Asteroid(randx+ 300, 0, 0.23f));

        }

    }

    public List<Asteroid> getAsteroids() { return asteroids; }

    public SpaceShip getSchiff() {return this.schiff;}




}
