package Model;

import java.awt.*;

public class SpaceShip {
    private int x;
    private final int y = 700;
    private final int h = 30;
    private final int w = 40;
    private boolean gameover = false;


    public SpaceShip(int x) {
        this.x = x;
    }

    public void move (int rechts, int links) {
        this.x += rechts;
        this.x -= links;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public void setGameover(boolean gameover) {
        this.gameover = gameover;
    }

    public Rectangle getSpaceShipBounds() {
        return new Rectangle(this.getX(), this.getY(), this.getH(), this.getW());
    }
}
