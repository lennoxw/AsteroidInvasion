package Model;

import java.awt.*;

public class Asteroid {

    private int x;
    private int y;
    private int h;
    private int w;
    private float speedX;

    public Asteroid(int x, int y, float speedX) {
        this.x = x;
        this.y = y;
        this.w = 30;
        this.h = 30;
        this.speedX = speedX;
    }

    public Asteroid createAsteroid(int x) {
        return new Asteroid(x, 0, 0.23f);
    }

    public boolean collision(SpaceShip schiff) {
        if (schiff.getX() > this.getX() - schiff.getW() + this.getW() &&
            schiff.getX() < this.getX() + schiff.getW() + this.getW() &&

            schiff.getY() > this.getY() - schiff.getH() + this.getH() &&
            schiff.getY() < this.getX() + schiff.getH() + this.getH()) {
            return true;
        }
        else {
            return false;

        }

    }

    public Rectangle getAsteroidBounds() {
        return new Rectangle(this.getX(), this.getY(), this.getH(), this.getW());
    }







    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getH() { return h;
    }

    public void setH(int h) {   this.h = h; }

    public int getW() { return w;    }

    public void setW(int w) {this.w = w;}

    public float getSpeedX() {return speedX;    }

    public void setSpeedX(float speedX) {
        this.speedX = speedX;
    }





    public void update(long elapsedTime) {
        this.y = Math.round(this.y + elapsedTime * speedX);
    }

}
